package main.mlp.core;

import java.util.List;

/**
 * 
 */
public interface Network {

	public abstract List<Double> feedForward(List<Double> input);

	public abstract void backPropagation(List<Double> error);

}