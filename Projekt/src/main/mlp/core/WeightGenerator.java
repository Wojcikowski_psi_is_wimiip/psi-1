package main.mlp.core;

/**
 * Interface for weight generator
 */
public interface WeightGenerator {

	Double next();
}
